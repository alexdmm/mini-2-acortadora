from django import forms
from .models import Contenido


class ContenidoForm(forms.ModelForm):
    class Meta:
        model = Contenido
        fields = ['url', 'url_short']

    widget = {
        'url': forms.TextInput(attrs={'class': 'form-control'}),
        'url_short': forms.TextInput(attrs={'class': 'form-control'})
    }
