from django.db import models

# Create your models here.


class Contenido(models.Model):
    url: str = models.CharField(max_length=200)
    short = models.CharField(max_length=100)


class Acortador(models.Model):
    short = models.CharField(max_length=100, blank=True, null=True)