from django.http import HttpResponse, Http404, HttpResponsePermanentRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from project.acorta.models import Acortador, Contenido
from forms import ContenidoForm


# Create your views here.


def checkFormat(url, short_url):
    if url.startswith("http://") or url.startswith("https://"):
        short_url = 'http://localhost:9999/' + short_url
        return url, short_url
    else:
        url = 'http://' + url
        short_url = 'http://localhost:9999/' + str(short_url)
        return url, short_url


def managEempty(short_url):
    shortEmpty = Acortador(short=short_url)
    shortEmpty.save()
    short_url = shortEmpty.id
    return short_url


def checkNewUrl(url, urls, short_url):
    if url in urls:
        content = Acortador.objects.get(short=short_url)
        content.short = short_url
        content.save()


@csrf_exempt
def index(request):
    formulario_vacio = ContenidoForm
    if request.method == 'POST':
        formulario = ContenidoForm(request.POST)
        if formulario.is_valid():
            short = formulario.cleaned_data.get('short', '')
            url = formulario.cleaned_data.get('url', '')

            if not short:
                short = managEempty(short)

            url, short = checkFormat(url, short)

            urls = Contenido.objects.values_list('url', flat=True)
            checkNewUrl(url, urls, short)

            respuestas_guardadas = Contenido.objects.all()
            contexto = {'respuestas_guardadas': respuestas_guardadas, 'formulario_vacio': formulario_vacio}
            return HttpResponse(render(request, 'acorta/formulario.html', contexto))
        else:
            raise Http404('La URL introducida no es válida')
    else:
        respuestas_guardadas = Contenido.objects.all()
        contexto = {'respuestas_guardadas': respuestas_guardadas, 'formulario_vacio': formulario_vacio}
        return render(request, 'acorta/formulario.html', contexto)


@csrf_exempt
def get_resource(request, recurso):
    try:
        recurso = f'http://localhost:8000/{recurso}'
        contenido = Contenido.objects.get(short=recurso)
        return HttpResponsePermanentRedirect(contenido.url)
    except Contenido.DoesNotExist:
        return handle_not_found()

def handle_not_found():
    return Http404('<h1>Recurso no disponible<h1>')
